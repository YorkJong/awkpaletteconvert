# @file PalCnv.awk
# Reads a color palette file (e.g. 64.pal) and converts it to an array of
# 0xVVUUYY values or an array of 0xBBGGRR values.
#
# @author Jiang Yu-Kuan <yukuan.jiang@gmail.com>
# @date 2015/10/27 (initial version)
# @date 2019/03/15 (last revision)
# @version 3.0

function version() {
    print "Palette value converter, palCnv v3.1"
    print "                 Jiang Yu-Kuan <yukuan.jiang@gmail.com>"
    print "                 2019/03/15"
}

function help() {
    print "Reads a color palette file (e.g. 64.pal) and converts it to an"
    print "array of 0xVVUUYY values or an array of 0xBBGGRR values."
    print ""
    print "Usage: PalCnv -v fmt=0xVVUUYY [OPTIONS] InputFile > OutputFile"
    print "       PalCnv -v fmt=0xBBGGRR [OPTIONS] InputFile > OutputFile"
    print "       PalCnv -v cmd=help"
    print "       PalCnv -v cmd=version"
    print "Options:"
    print "    -v fmt=FORMAT    assign a value format (0xBBGGRR or 0xVVUUYY)"
    print "    -v fields=N      assign the number of fields of a line",
                               "(default is 4)"
    print "    -v indent=N      assign indent width (default is 8)"
    print "    -v cmd=help      show this help message and exit"
    print "    -v cmd=version   show the version message and exit"
}


BEGIN {
    if (cmd == "version") {
        version()
        exit
    }
    if ((cmd == "help") || ((fmt != "0xBBGGRR") && (fmt != "0xVVUUYY"))) {
        help()
        exit
    }

    FS = "[[:space:]]*,[[:space:]]*"
    RS = ",?[[:space:]]*\n"

    if (fields == "")
        fields = 4          # fields of a line
    if (indent == "")
        indent = 8          # indent width
}

isnum($1) && isnum($2) && isnum($3) {
    if ((i % fields) == 0)
        printf "%*s", indent, " "

    r = $1
    g = $2
    b = $3

    if (fmt == "0xBBGGRR")
        print0xBBGGRR(r, g, b)
    else if (fmt == "0xVVUUYY")
        print0xVVUUYY(r, g, b)

    ++i
    if ((i % fields) == 0)
        printf "\n"
}

END {}


#-------------------------------------------------------------------------------
# Help Functions
#-------------------------------------------------------------------------------

function isnum(x) {
    return (x ~ /^[[:space:]]*[[:digit:]]+$/) ||
           (x ~ /^[[:space:]]*0[xX][[:xdigit:]]+$/)
}


function print0xBBGGRR(r, g, b) {
    printf "0x%02X%02X%02X, ", b, g, r
}


function print0xVVUUYY(r, g, b) {
    y = int(b*0.114   + g*0.587   + r*0.299)
    u = int(b*0.5     + g*-0.3313 + r*-0.1687 + 128)
    v = int(b*-0.0813 + g*-0.4187 + r*0.5     + 128)
    printf "0x%02X%02X%02X, ", v, u, y
}


#-------------------------------------------------------------------------------

