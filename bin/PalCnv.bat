:: @file PalCnv.bat
:: Reads a color palette file (e.g., 64.pal) and converts it to an array of
:: 0xBBGGRR values and an array of 0xVVUUYY values.
::
:: @author Jiang Yu-Kuan <york_jiang@mars-semi.com.tw>
:: @date 2015/10/27 (initial version)
:: @date 2015/11/30 (last revision)
:: @version 3.0
::
:: Usage
:: -----
:: Drags a palatte file (e.g. 64.pal or 16.pal) to the batch file and you'll
:: get a *.bbggrr.txt (e.g. 64.pal.bbggrr.txt) file and *.vvuuyy.txt (e.g.
:: 64.pal.vvuuyy.txt) file.

@echo off
set PalCnv=PalCnv.exe

set bat_dir=%~dp0
cd /D %bat_dir%

set infile=%1
if "%infile%" == "" (
    set infile=64.pal
)


@echo ^> Help
%PalCnv%
echo:

echo:
set outfile=%infile%.bbggrr.txt
echo ^> Get file (%outfile%) of 0xBBGGRR values
%PalCnv% -v fmt=0xBBGGRR %infile% > %outfile%

echo:
set outfile=%infile%.vvuuyy.txt
echo ^> Get file (%outfile%) of 0xVVUUYY values
%PalCnv% -v fmt=0xVVUUYY %infile% > %outfile%


pause
