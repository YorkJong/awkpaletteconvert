# PaletteConvert #

PaletteConvert is an AWK application. It reads a color palette file (e.g.
*64.pal*) and converts it to an array of *0xVVUUYY* values or an array of
*0xBBGGRR* values.

For the ease of use on Windows, the AWK script is compiled into an EXE manner
and is wrapped into a batch file.


## Install ##

1. Download a binary distribution (e.g., *PaletteConvert-3.0-bin.zip*) from
   [Downloads](https://bitbucket.org/YorkJong/awkpaletteconvert/downloads) page.
2. Uncompress the binary distribution.


## Getting Started ##

1. Install PalleteConvert.
2. Drag a palatte file (e.g., *64.pal* or *16.pal*) to *PalCnv.bat* and you'll
   get a `*.bbggrr.txt` (e.g., *64.pal.bbggrr.txt*) file and `*.vvuuyy.txt`
   (e.g., *64.pal.vvuuyy.txt*) file.


## An Example ##
### Let's say that I have a color palette file **16.pal**:
```
128, 0, 0,
255, 255, 255,
180, 206, 252,
164, 181, 209,
108, 158, 252,
36, 214, 84,
100, 114, 139,
52, 138, 76,
36, 110, 52,
76, 86, 100,
76, 74, 76,
244, 58, 44,
156, 62, 52,
124, 42, 36,
44, 43, 44,
0, 0, 0,
```
A color palette file is a text file that lists color values. Each color is a
comma-separated values in sequence of R, G, B.


### I drag the *16.pal* file to *PalCnv.bat* to generate the following 2 files:
**16.pal.bbggrr.txt**:
```
        0xC06A26, 0x8080FE, 0x6F9BCB, 0x7590B3,
        0x5FB799, 0x315D91, 0x768E70, 0x5A6F69,
        0x5F6F51, 0x798854, 0x80804A, 0xDE5970,
        0xAF6B58, 0xA96F41, 0x80802B, 0x808000,
```
Each color is converted into 0xBBGGRR manner in the above file.

**16.pal.vvuuyy.txt**:
```
        0xC06A26, 0x8080FE, 0x6F9BCB, 0x7590B3,
        0x5FB799, 0x315D91, 0x768E70, 0x5A6F69,
        0x5F6F51, 0x798854, 0x80804A, 0xDE5970,
        0xAF6B58, 0xA96F41, 0x80802B, 0x808000,
```
Each color is converted into 0xVVUUYY manner in the above file.


## Command Line ##
Type `PalCnv.exe -v cmd=help` in a console window will get the following
help messages:
```
Reads a color palette file (e.g., 64.pal) and converts it to an
array of 0xVVUUYY values or an array of 0xBBGGRR values.

Usage: PalCnv.exe -v fmt=0xVVUUYY [OPTIONS] InputFile > OutputFile
       PalCnv.exe -v fmt=0xBBGGRR [OPTIONS] InputFile > OutputFile
       PalCnv.exe -v cmd=help
       PalCnv.exe -v cmd=version
Options:
    -v fmt=FORMAT    assign a value format (0xBBGGRR or 0xVVUUYY)
    -v fields=N      assign the number of fields of a line (default is 4)
    -v indent=N      assign indent width (default is 8)
    -v cmd=help      show this help message and exit
    -v cmd=version   show the version message and exit
```
